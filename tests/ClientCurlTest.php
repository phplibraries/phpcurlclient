<?php
declare(strict_types=1);

namespace PHPLibraries\PhpCurlClient\Tests;

use PHPLibraries\PhpCurlClient\ClientCurl;
use PHPLibraries\PhpCurlClient\Exception\CurlException;

class ClientCurlTest extends \Codeception\Test\Unit
{

    /**
     * @return void
     * @throws CurlException
     */
    public function testGetMethodSuccess()
    {
        $client = new ClientCurl();
        $response = $client->get('https://jsonplaceholder.typicode.com/todos/1');
        $this->assertGreaterThan(0, (int)$response->getHeaderLine('content-length'));
        $this->assertEquals('application/json; charset=utf-8', $response->getHeaderLine('content-type'));
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @return void
     * @throws CurlException
     */
    public function testGetMethodNoSuccess404Code()
    {
        $client = new ClientCurl();
        $response = $client->get('https://jsonplaceholder.typicode.com/todos1/1');
        $this->assertGreaterThan(0, (int)$response->getHeaderLine('content-length'));
        $this->assertEquals('application/json; charset=utf-8', $response->getHeaderLine('content-type'));
        $this->assertEquals(404, $response->getStatusCode());

    }

    /**
     * @return void
     * @throws CurlException
     */
    public function testPostMethodSuccess()
    {
        $client = new ClientCurl();
        $body = json_encode([
            'title' => 'foo',
            'body' => 'bar',
            'userId' => 1,
        ]);
        $response = $client->post('https://jsonplaceholder.typicode.com/posts', $body, ['Content-type' => 'application/json; charset=UTF-8']);

        $this->assertGreaterThan(0, (int)$response->getHeaderLine('content-length'));
        $this->assertEquals('application/json; charset=utf-8', $response->getHeaderLine('content-type'));
        $this->assertEquals(201, $response->getStatusCode());

        $responseContent = json_decode($response->getBody()->getContents(), true);

        $this->assertEquals('foo', $responseContent['title'] ?? null);
        $this->assertEquals('bar', $responseContent['body'] ?? null);
        $this->assertEquals(1, $responseContent['userId'] ?? null);
    }

    /**
     * @return void
     * @throws CurlException
     */
    public function testPostMethodNoUpdateDataWithBadContentType()
    {
        $client = new ClientCurl();
        $body = json_encode([
            'title' => 'foo',
            'body' => 'bar',
            'userId' => 1,
        ]);
        $response = $client->post('https://jsonplaceholder.typicode.com/posts', $body, ['Content-type' => 'application1; charset=UTF-8']);

        $this->assertGreaterThan(0, (int)$response->getHeaderLine('content-length'));
        $this->assertEquals('application/json; charset=utf-8', $response->getHeaderLine('content-type'));
        $this->assertEquals(201, $response->getStatusCode());

        $responseContent = json_decode($response->getBody()->getContents(), true);
        $this->assertNotEquals(1, $responseContent['userId'] ?? null);
    }

    /**
     * @return void
     * @throws CurlException
     */
    public function testPutMethodSuccess()
    {
        $client = new ClientCurl();
        $body = json_encode([
            'title' => 'foo1',
            'body' => 'bar',
            'userId' => 1,
        ]);
        $response = $client->put('https://jsonplaceholder.typicode.com/posts/1', $body, ['Content-type' => 'application/json; charset=UTF-8']);

        $this->assertGreaterThan(0, (int)$response->getHeaderLine('content-length'));
        $this->assertEquals('application/json; charset=utf-8', $response->getHeaderLine('content-type'));
        $this->assertEquals(200, $response->getStatusCode());

        $responseContent = json_decode($response->getBody()->getContents(), true);

        $this->assertEquals('foo1', $responseContent['title'] ?? null);
        $this->assertEquals('bar', $responseContent['body'] ?? null);
        $this->assertEquals(1, $responseContent['userId'] ?? null);
    }

    /**
     * @return void
     * @throws CurlException
     */
    public function testPatchMethodSuccess()
    {
        $client = new ClientCurl();

        $body = json_encode([
            'title' => 'foo2',
        ]);

        $response = $client->patch('https://jsonplaceholder.typicode.com/posts/1', $body, ['Content-type' => 'application/json; charset=UTF-8']);

        $this->assertGreaterThan(0, (int)$response->getHeaderLine('content-length'));
        $this->assertEquals('application/json; charset=utf-8', $response->getHeaderLine('content-type'));
        $this->assertEquals(200, $response->getStatusCode());

        $responseContent = json_decode($response->getBody()->getContents(), true);

        $this->assertEquals('foo2', $responseContent['title'] ?? null);
        $this->assertEquals(1, $responseContent['id'] ?? null);
    }

    /**
     * @return void
     * @throws CurlException
     */
    public function testDeleteMethodSuccess()
    {
        $client = new ClientCurl();

        $response = $client->delete('https://jsonplaceholder.typicode.com/posts/1');

        $this->assertGreaterThan(0, (int)$response->getHeaderLine('content-length'));
        $this->assertEquals('application/json; charset=utf-8', $response->getHeaderLine('content-type'));
        $this->assertEquals(200, $response->getStatusCode());

        $responseContent = json_decode($response->getBody()->getContents(), true);
        $this->assertEmpty($responseContent);
    }
}