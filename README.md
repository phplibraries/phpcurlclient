# PHPCurlClient

## Getting started

## Installation
This project using composer.
```
$ composer require phplibraries/phpcurlclient
```

##  Requirements
* PHP 8.2
* Guzzlehttp/psr
* ext-curl

## Quick Start and Examples

```php
use PHPLibraries\PhpCurlClient\ClientCurl;


$curlClient = new Curl();
$client->setJwtAuthenticateToken('jwtAuthenticateToken');
$response = $client->get('https://jsonplaceholder.typicode.com/todos/1', ['Content-Type' => 'application/xml']);
$jsonBody = $response->getBody()->getContents());
```

## Tests
### Required
* codeception/codeception 5.0.x-dev
* codeception/module-asserts

### Run tests

```
php vendor/bin/codecept run unit
```