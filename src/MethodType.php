<?php
declare(strict_types=1);

namespace PHPLibraries\PhpCurlClient;

enum MethodType:string
{
    case GET = 'GET';
    case POST = 'POST';
    case PUT = 'PUT';
    case PATCH = 'PATCH';
    case DELETE = 'DELETE';
}