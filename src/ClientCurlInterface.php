<?php

namespace PHPLibraries\PhpCurlClient;

use GuzzleHttp\Psr7\Response;

interface ClientCurlInterface
{
    public function get(string $uri, array $headers): Response;
    public function post(string $uri, $body, array $headers): Response;
    public function put(string $uri, $body, array $headers): Response;
    public function delete(string $uri, array $headers): Response;
    public function patch(string $uri, $body, array $headers): Response;
    public function setJwtAuthenticateToken(?string $token): self;
}