<?php
declare(strict_types=1);

namespace PHPLibraries\PhpCurlClient;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class MessageFactory
{
    static function createRequest(MethodType $method, $uri, array $headers = [], $body = null, $version = '1.1'): Request
    {
        return new Request($method->value, $uri, $headers, $body, $version);
    }

    static function createResponse($status = 200, array $headers = [], $body = null, $version = '1.1', $reason = null): Response
    {
        return new Response($status, $headers, $body, $version , $reason);
    }
}