<?php

namespace PHPLibraries\PhpCurlClient\Helper;

use GuzzleHttp\Psr7\Request;

class RequestHelper
{
    /**
     * @param Request $request
     * @return array
     */
    static function getOptHeaders(Request $request): array
    {
        $headers = $request->getHeaders();
        $optHeaders = [];
        foreach ($headers as $key => $header){
            $optHeaders[] = sprintf('%s: %s', $key, implode(',', $header));
        }
        return $optHeaders;
    }
}