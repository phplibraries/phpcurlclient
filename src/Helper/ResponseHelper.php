<?php
declare(strict_types=1);

namespace PHPLibraries\PhpCurlClient\Helper;

class ResponseHelper
{
    /**
     * @param string $header
     * @return array|null
     */
    static function getArrayHeaderFromString(string $header): ?array
    {
        $header = explode(':', $header, 2);
        if (count($header) < 2)
            return null;
        return [strtolower(trim($header[0])) => trim($header[1])];
    }
}