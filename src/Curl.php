<?php
declare(strict_types=1);

namespace PHPLibraries\PhpCurlClient;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PHPLibraries\PhpCurlClient\Exception\CurlException;
use PHPLibraries\PhpCurlClient\Helper\RequestHelper;
use PHPLibraries\PhpCurlClient\Helper\ResponseHelper;

abstract class Curl
{
    /**
     * @var null
     */
    private $ch = null;


    /**
     * @var array
     */
    private array $responseHeaders = [];

    /**
     * @var Response|null
     */
    private ?Response $response = null;

    /**
     * @param Request $request
     * @return Response
     * @throws CurlException
     */
    protected function send(Request $request): Response
    {
        $this->reset();

        $this->initOpts($request);

        curl_exec($this->ch);
        $error = curl_errno($this->ch);

        if ($error !== 0) {
            curl_close($this->ch);
            throw new CurlException(curl_error($this->ch));
        }
        curl_close($this->ch);
        return $this->response;
    }

    /**
     * @return void
     */
    private function reset(): void
    {
        $this->ch = curl_init();
    }

    /**
     * @param Request $request
     * @return void
     */
    private function initOpts(Request $request): void
    {
        $body = $request->getBody();
        $bodySize = $body->getSize();

        $options = [
            CURLOPT_URL => strval($request->getUri()),
            CURLOPT_CUSTOMREQUEST => $request->getMethod(),
            CURLOPT_HEADER => true,
            CURLOPT_HTTPHEADER => RequestHelper::getOptHeaders($request),
        ];


        if ($bodySize !== 0) {
            $options[CURLOPT_POSTFIELDS] = $body->getContents();
            $options[CURLOPT_POST] = true;
        }

        curl_setopt($this->ch, CURLOPT_WRITEFUNCTION, array($this, 'setResponseCallback'));
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, array($this, 'setHeaderCallback'));
        curl_setopt_array($this->ch, $options);
    }

    /**
     * @param $ch
     * @param $str
     * @return int
     */
    private function setHeaderCallback($ch, $header): int
    {
        $result = ResponseHelper::getArrayHeaderFromString($header);
        if ($result) {
            $this->responseHeaders[key($result)] = $result[key($result)];
        }
        return strlen($header);
    }

    /**
     * @param $ch
     * @param $str
     * @return int
     */
    private function setResponseCallback($ch, $str): int
    {
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $headers = $this->responseHeaders;
        $body = $str;
        $version = (string)curl_getinfo($ch, CURLINFO_HTTP_VERSION);
        $this->response = MessageFactory::createResponse($status, $headers, $body, $version);
        return strlen($str);
    }
}