<?php
declare(strict_types=1);

namespace PHPLibraries\PhpCurlClient;

use PHPLibraries\PhpCurlClient\Exception\CurlException;
use GuzzleHttp\Psr7\Response;

class ClientCurl extends Curl implements ClientCurlInterface
{

    /**
     * @var array
     */
    private array $optionalHeaders = [];

    /**
     * @param string $uri
     * @param array $headers
     * @return Response
     * @throws CurlException
     */
    public function get(string $uri, array $headers= []): Response
    {
        $request = MessageFactory::createRequest(MethodType::GET, $uri, array_merge($headers, $this->optionalHeaders));
        return $this->send($request);
    }

    /**
     * @param string $uri
     * @param $body
     * @param array $headers
     * @return Response
     * @throws CurlException
     */
    public function post(string $uri, $body, array $headers = []): Response
    {
        $request = MessageFactory::createRequest(MethodType::POST, $uri, array_merge($headers, $this->optionalHeaders), $body);
        return $this->send($request);
    }

    /**
     * @param string $uri
     * @param $body
     * @param $headers
     * @return Response
     * @throws CurlException
     */
    public function put(string $uri, $body, array $headers = []): Response
    {
        $request = MessageFactory::createRequest(MethodType::PUT, $uri, array_merge($headers, $this->optionalHeaders), $body);
        return $this->send($request);
    }

    /**
     * @param string $uri
     * @param array $headers
     * @return Response
     * @throws CurlException
     */
    public function delete(string $uri, array $headers = []): Response
    {
        $request = MessageFactory::createRequest(MethodType::DELETE, $uri, array_merge($headers, $this->optionalHeaders));
        return $this->send($request);
    }

    /**
     * @param string $uri
     * @param $body
     * @param array $headers
     * @return Response
     * @throws CurlException
     */
    public function patch(string $uri, $body, array $headers = []): Response
    {
        $request = MessageFactory::createRequest(MethodType::PATCH, $uri, array_merge($headers, $this->optionalHeaders), $body);
        return $this->send($request);
    }

    /**
     * @param string|null $token
     * @return $this
     */
    public function setJwtAuthenticateToken(?string $token): self
    {
        $this->optionalHeaders['Authorization'] = sprintf('Bearer %s', $token);
        return $this;
    }
}